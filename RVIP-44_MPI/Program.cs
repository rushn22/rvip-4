﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPI;

namespace RVIP_44_MPI
{
    class Program
    {
        static void Main(string[] args)
        {
            using (new MPI.Environment(ref args))
            {
                if (Communicator.world.Rank == 0)
                {
                    double[,] mas = new double[2, 2];
                    mas[0, 0] = 2;
                    mas[0, 1] = 2;
                    mas[1, 0] = 4;
                    mas[1, 1] = 2;
                     
                    double max = 0;
                     
                    for (int i = 0; i < mas.GetLength(0); i++)
                    {
                        for (int j = 0; j < mas.GetLength(1); j++)
                        {
                            if (mas[i, j] >= max)
                            {
                                max = mas[i, j];
                            }
                        }
                    }
                    Console.WriteLine("Массив ДО преобразований:");
                    for (int i = 0; i < mas.GetLength(0); i++)
                    {
                        for (int j = 0; j < mas.GetLength(1); j++)
                        {
                            Console.Write(mas[i, j] + " ");
                        }
                        Console.WriteLine();
                    }
                    Console.WriteLine("Максимальный элемент: " + max);
                    for (int i = 0; i < mas.GetLength(0); i++)
                    {
                        for (int j = 0; j < mas.GetLength(1); j++)
                        {
                            mas[i, j] = mas[i, j] / max;
                        }
                    }

                    
                    Communicator.world.Send(mas, 1, 0);
                }
            

                if (Communicator.world.Rank == 1)
                {
                    double[,] msg = Communicator.world.Receive<double[,]>(Communicator.world.Rank-1, 0);
                    Console.WriteLine("Массив ПОСЛЕ преобразований:");
                    for (int i = 0; i < msg.GetLength(0); i++)
                    {
                        for (int j = 0; j < msg.GetLength(1); j++)
                        {
                            Console.Write(msg[i, j] + " ");
                        }
                        Console.WriteLine();
                    }
                }
               
            }
            
            
        }
    }
}
